<?php

namespace Officient\Notifier\Symfony;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class NotifierBundle
 *
 * This class just needs to be here, because of stuff
 * in symfony documentation...
 *
 * @package Officient\Notifier\Symfony
 */
class NotifierBundle extends Bundle
{
    //
}