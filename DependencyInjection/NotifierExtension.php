<?php

namespace Officient\Notifier\Symfony\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;

class NotifierExtension extends Extension
{
    /**
     * @inheritDoc
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new YamlFileLoader(
            $container,
            new FileLocator(__DIR__.'/../Resources/config')
        );
        $loader->load('services.yaml');

        $apiBus = $container->getDefinition('Officient\Notifier\Bus\ApiBus');
        $apiBus->replaceArgument(0, $config['host'] ?? null);
        $apiBus->replaceArgument(1, $config['port'] ?? null);
        $apiBus->replaceArgument(2, $config['owner_token'] ?? null);

        $notifier = $container->getDefinition('Officient\Notifier\Notifier');
        $notifier->replaceArgument(1, $config['mailgun_domain'] ?? null);
    }
}