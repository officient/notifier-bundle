<?php

namespace Officient\Notifier\Symfony\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    /**
     * @inheritDoc
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder('notifier');

        $treeBuilder->getRootNode()
            ->children()
                ->scalarNode('host')->isRequired()->end()
                ->integerNode('port')->isRequired()->end()
                ->scalarNode('owner_token')->isRequired()->end()
                ->scalarNode('mailgun_domain')->isRequired()->end()
            ->end();

        return $treeBuilder;
    }
}